diff --git a/handler/pom.xml b/handler/pom.xml
index 69af32a..b9e5596 100644
--- a/handler/pom.xml
+++ b/handler/pom.xml
@@ -54,11 +54,6 @@
       <artifactId>bcpkix-jdk15on</artifactId>
       <optional>true</optional>
     </dependency>
-    <dependency>
-      <groupId>org.eclipse.jetty.alpn</groupId>
-      <artifactId>alpn-api</artifactId>
-      <optional>true</optional>
-    </dependency>
     <dependency>
       <groupId>org.mockito</groupId>
       <artifactId>mockito-core</artifactId>
diff --git a/handler/src/main/java/io/netty/handler/ssl/JdkAlpnApplicationProtocolNegotiator.java b/handler/src/main/java/io/netty/handler/ssl/JdkAlpnApplicationProtocolNegotiator.java
index 9c4ab9e..5cc1ab7 100644
--- a/handler/src/main/java/io/netty/handler/ssl/JdkAlpnApplicationProtocolNegotiator.java
+++ b/handler/src/main/java/io/netty/handler/ssl/JdkAlpnApplicationProtocolNegotiator.java
@@ -21,7 +21,7 @@ import javax.net.ssl.SSLEngine;
  * The {@link JdkApplicationProtocolNegotiator} to use if you need ALPN and are using {@link SslProvider#JDK}.
  */
 public final class JdkAlpnApplicationProtocolNegotiator extends JdkBaseApplicationProtocolNegotiator {
-    private static final boolean AVAILABLE = JettyAlpnSslEngine.isAvailable();
+    private static final boolean AVAILABLE = false;
     private static final SslEngineWrapperFactory ALPN_WRAPPER = AVAILABLE ? new AlpnWrapper() : new FailureWrapper();
 
     /**
@@ -121,10 +121,6 @@ public final class JdkAlpnApplicationProtocolNegotiator extends JdkBaseApplicati
         @Override
         public SSLEngine wrapSslEngine(SSLEngine engine, JdkApplicationProtocolNegotiator applicationNegotiator,
                                        boolean isServer) {
-            if (JettyAlpnSslEngine.isAvailable()) {
-                return isServer ? JettyAlpnSslEngine.newServerEngine(engine, applicationNegotiator)
-                        : JettyAlpnSslEngine.newClientEngine(engine, applicationNegotiator);
-            }
             throw new RuntimeException("Unable to wrap SSLEngine of type " + engine.getClass().getName());
         }
     }
diff --git a/handler/src/main/java/io/netty/handler/ssl/JettyAlpnSslEngine.java b/handler/src/main/java/io/netty/handler/ssl/JettyAlpnSslEngine.java
deleted file mode 100644
index 624719a..0000000
--- a/handler/src/main/java/io/netty/handler/ssl/JettyAlpnSslEngine.java
+++ /dev/null
@@ -1,158 +0,0 @@
-/*
- * Copyright 2014 The Netty Project
- *
- * The Netty Project licenses this file to you under the Apache License,
- * version 2.0 (the "License"); you may not use this file except in compliance
- * with the License. You may obtain a copy of the License at:
- *
- *   http://www.apache.org/licenses/LICENSE-2.0
- *
- * Unless required by applicable law or agreed to in writing, software
- * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
- * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
- * License for the specific language governing permissions and limitations
- * under the License.
- */
-package io.netty.handler.ssl;
-
-import static io.netty.handler.ssl.SslUtils.toSSLHandshakeException;
-import static io.netty.util.internal.ObjectUtil.checkNotNull;
-
-import io.netty.handler.ssl.JdkApplicationProtocolNegotiator.ProtocolSelectionListener;
-import io.netty.handler.ssl.JdkApplicationProtocolNegotiator.ProtocolSelector;
-
-import java.util.LinkedHashSet;
-import java.util.List;
-
-import javax.net.ssl.SSLEngine;
-import javax.net.ssl.SSLException;
-
-import io.netty.util.internal.PlatformDependent;
-import org.eclipse.jetty.alpn.ALPN;
-
-abstract class JettyAlpnSslEngine extends JdkSslEngine {
-    private static final boolean available = initAvailable();
-
-    static boolean isAvailable() {
-        return available;
-    }
-
-    private static boolean initAvailable() {
-        // TODO: Add support for ALPN when using Java9 and still be able to configure it the Netty way.
-        if (PlatformDependent.javaVersion() <= 8) {
-            try {
-                // Always use bootstrap class loader.
-                Class.forName("sun.security.ssl.ALPNExtension", true, null);
-                return true;
-            } catch (Throwable ignore) {
-                // alpn-boot was not loaded.
-            }
-        }
-        return false;
-    }
-
-    static JettyAlpnSslEngine newClientEngine(SSLEngine engine,
-            JdkApplicationProtocolNegotiator applicationNegotiator) {
-        return new ClientEngine(engine, applicationNegotiator);
-    }
-
-    static JettyAlpnSslEngine newServerEngine(SSLEngine engine,
-            JdkApplicationProtocolNegotiator applicationNegotiator) {
-        return new ServerEngine(engine, applicationNegotiator);
-    }
-
-    private JettyAlpnSslEngine(SSLEngine engine) {
-        super(engine);
-    }
-
-    private static final class ClientEngine extends JettyAlpnSslEngine {
-        ClientEngine(SSLEngine engine, final JdkApplicationProtocolNegotiator applicationNegotiator) {
-            super(engine);
-            checkNotNull(applicationNegotiator, "applicationNegotiator");
-            final ProtocolSelectionListener protocolListener = checkNotNull(applicationNegotiator
-                            .protocolListenerFactory().newListener(this, applicationNegotiator.protocols()),
-                    "protocolListener");
-            ALPN.put(engine, new ALPN.ClientProvider() {
-                @Override
-                public List<String> protocols() {
-                    return applicationNegotiator.protocols();
-                }
-
-                @Override
-                public void selected(String protocol) throws SSLException {
-                    try {
-                        protocolListener.selected(protocol);
-                    } catch (Throwable t) {
-                        throw toSSLHandshakeException(t);
-                    }
-                }
-
-                @Override
-                public void unsupported() {
-                    protocolListener.unsupported();
-                }
-            });
-        }
-
-        @Override
-        public void closeInbound() throws SSLException {
-            try {
-                ALPN.remove(getWrappedEngine());
-            } finally {
-                super.closeInbound();
-            }
-        }
-
-        @Override
-        public void closeOutbound() {
-            try {
-                ALPN.remove(getWrappedEngine());
-            } finally {
-                super.closeOutbound();
-            }
-        }
-    }
-
-    private static final class ServerEngine extends JettyAlpnSslEngine {
-        ServerEngine(SSLEngine engine, final JdkApplicationProtocolNegotiator applicationNegotiator) {
-            super(engine);
-            checkNotNull(applicationNegotiator, "applicationNegotiator");
-            final ProtocolSelector protocolSelector = checkNotNull(applicationNegotiator.protocolSelectorFactory()
-                            .newSelector(this, new LinkedHashSet<String>(applicationNegotiator.protocols())),
-                    "protocolSelector");
-            ALPN.put(engine, new ALPN.ServerProvider() {
-                @Override
-                public String select(List<String> protocols) throws SSLException {
-                    try {
-                        return protocolSelector.select(protocols);
-                    } catch (Throwable t) {
-                        throw toSSLHandshakeException(t);
-                    }
-                }
-
-                @Override
-                public void unsupported() {
-                    protocolSelector.unsupported();
-                }
-            });
-        }
-
-        @Override
-        public void closeInbound() throws SSLException {
-            try {
-                ALPN.remove(getWrappedEngine());
-            } finally {
-                super.closeInbound();
-            }
-        }
-
-        @Override
-        public void closeOutbound() {
-            try {
-                ALPN.remove(getWrappedEngine());
-            } finally {
-                super.closeOutbound();
-            }
-        }
-    }
-}
diff --git a/handler/src/test/java/io/netty/handler/ssl/JdkSslEngineTest.java b/handler/src/test/java/io/netty/handler/ssl/JdkSslEngineTest.java
index e32fa0d..a8014e5 100644
--- a/handler/src/test/java/io/netty/handler/ssl/JdkSslEngineTest.java
+++ b/handler/src/test/java/io/netty/handler/ssl/JdkSslEngineTest.java
@@ -62,7 +62,7 @@ public class JdkSslEngineTest extends SSLEngineTest {
         ALPN_DEFAULT {
             @Override
             boolean isAvailable() {
-                return JettyAlpnSslEngine.isAvailable();
+                return false;
             }
 
             @Override
